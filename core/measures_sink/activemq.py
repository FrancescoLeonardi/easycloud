##
## Python module for publishing measures to Apache ActiveMQ.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from core import measures_sink
import stomp
import json
import logging


class ActiveMQSink(measures_sink.MeasuresSink):
    """Measures sink that interfaces with Apache ActiveMQ
       (https://activemq.apache.org/).

    Tested with Apache ActiveMQ v. 5.15.12 and Stomp.py v. 6.0.0.

    Note, if you use an older version of Apache ActiveMQ you have to suitably
    configure it in order to use STOPM, as described at the following URL:
        http://activemq.apache.org/stomp.html.

    References:
    - Apache ActiveMQ: https://activemq.apache.org
    - Stomp.py: https://github.com/jasonrbriggs/stomp.py
    """

    # See https://activemq.apache.org/how-does-a-queue-compare-to-a-topic
    QUEUE_DESTINATION = 'queue' # 1-to-1 communications (load balancer semantics)
    TOPIC_DESTINATION = 'topic' # 1-to-many communications (publish-subscribe semantics)

    DEFAULT_BROKERS = [('localhost',61613)]
    #DEFAULT_USER = 'admin'
    DEFAULT_USER = None
    #DEFAULT_PASSWORD = 'admin'
    DEFAULT_PASSWORD = None
    DEFAULT_DESTINATION = 'easycloud'
    DEFAULT_DESTINATION_TYPE = TOPIC_DESTINATION

    @classmethod
    def from_conf(cls, conf):
        brokers = None
        if 'brokers' in conf:
            brokers = [ tuple(hp.strip().split(':')) for hp in conf['brokers'].split(',') ]
        user = None
        if 'user' in conf:
            user = conf['user']
        password = None
        if 'password' in conf:
            password = conf['password']
        destination = None
        if 'destination' in conf:
            destination = conf['destination']
        destination_type = None
        if 'destination_type' in conf:
            destination = conf['destination_type'].lower()
        return cls(brokers=brokers, user=user, password=password, destination=destination, destination_type=destination_type)

    def __init__(self, brokers=DEFAULT_BROKERS, user=DEFAULT_USER, password=DEFAULT_PASSWORD, destination=DEFAULT_DESTINATION, destination_type=DEFAULT_DESTINATION_TYPE):
        super(ActiveMQSink, self).__init__()
        self._brokers = brokers if (brokers is not None) and len(brokers) > 0 else self.DEFAULT_BROKERS
        self._user = user if (user is not None) and len(user) > 0 else self.DEFAULT_USER
        self._password = password if (password is not None) and len(password) > 0 else self.DEFAULT_PASSWORD
        self._dest = destination if (destination is not None) and len(destination) > 0 else self.DEFAULT_DESTINATION
        self._dest_type = destination_type if (destination_type is not None) and len(destination_type) > 0 else self.DEFAULT_DESTINATION_TYPE
        if self._dest_type not in (self.QUEUE_DESTINATION, self.TOPIC_DESTINATION):
            self._dest_type = self.DEFAULT_DESTINATION_TYPE
        if self._dest_type == self.QUEUE_DESTINATION:
            self._dest = '/queue/' + self._dest
        elif self._dest_type == self.TOPIC_DESTINATION:
            self._dest = '/topic/' + self._dest

    def put(self, measure):
        #msg_key = self._measure_group_encode(self._measure_group(measure))
        msg = self._make_message(measure)
        try:
            conn = stomp.Connection(host_and_ports=self._brokers)
            conn.connect(self._user, self._password, wait=True)
            conn.send(body=msg, destination=self._dest)
            conn.disconnect()
        except Exception as e:
            logging.warn('Unable to deliver message: {}'.format(e))

    def mput(self, measures):
        # Groups measures by object namespace, object ID and metric to optimize writes on CSV files
        measures_groups = dict() # {namespace => {object-id => {metric => [measure1, measure2, ...]}}}
        #for measure in measures:
        #    group_id = self._measure_group_encode(self._measure_group(measure))
        #    if group_id not in measures_groups:
        #        measures_groups[group_id] = []
        #    measures_groups[group_id].append(measure)
        #try:
        #    conn = stomp.Connection(host_and_ports=self._brokers)
        #    conn.connect(self._user, self._password, wait=True)
        #    txid = conn.begin()
        #    for group_id in measures_groups:
        #        #msg_key = group_id
        #        for measure in measures_groups[group_id]:
        #            msg = self._make_message(measure)
        #            conn.send(self._dest, msg, content_type='application/json', transaction=txid)
        #    conn.commit(txid)
        #    conn.disconnect()
        #except stomp.exception.StompException as e:
        #    logging.warn('Unable to deliver message: {}'.format(e))
        try:
            conn = stomp.Connection(host_and_ports=self._brokers)
            conn.connect(self._user, self._password, wait=True)
            txid = conn.begin()
            for measure in measures:
                msg = self._make_message(measure)
                conn.send(self._dest, msg, content_type='application/json', transaction=txid)
            conn.commit(txid)
            conn.disconnect()
        except stomp.exception.StompException as e:
            logging.warn('Unable to deliver message: {}'.format(e))

    def _make_message(self, measure):
        return json.dumps({'timestamp': measure.timestamp.isoformat(),
                           'object-id': measure.object_id,
                           'object_ns': measure.object_ns,
                           'metric': measure.metric,
                           'unit': measure.unit,
                           'value': measure.value})

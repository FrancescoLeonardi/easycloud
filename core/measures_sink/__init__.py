##
## Python module for common measure sink types and functions.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from abc import ABC, abstractmethod
import base64
from datetime import datetime


class Measure:
    """A measure to be sent to sinks. """

    DEFAULT_NAMESPACE = "generic"

    def __init__(self, object_id=None, object_ns=DEFAULT_NAMESPACE, metric=None, timestamp=datetime.now(), unit=None, value=None):
        """Creates a new measure.

        Args:
            object_id (str): the object ID for which the measure has been collected (e.g., an instance ID).
            object_ns (str): the object namespace under which object_id is unique (e.g., 'aws_eu-central-1',
                'openstack-region1', etc.). Default to 'generic'.
            metric (str): the metric name.
            timestamp (datetime): the timestamp of when this measure has been collected.
            unit (str): the unit of measure (e.g., %, GB, etc.)
            value (any): the value collected for this metric.
        """
        self.object_id = object_id
        self.object_ns = object_ns
        self.metric = metric
        self.timestamp = timestamp
        self.unit = unit
        self.value = value

    def __str__(self):
        return '<timestamp: {}, object-ns: {}, object-id: {}, metric: {}, unit: {}, value: {}>'.format(self.timestamp.isoformat() if self.timestamp is not None else 'na', self.object_ns, self.object_id, self.metric, self.unit, self.value)


class MeasuresSink(ABC):
    """Model a sink for measures."""

    _MEASURE_GROUP_SEP = ':'
    #_MEASURE_GROUP_SEP_B = b'-'

    @classmethod
    @abstractmethod
    def from_conf(cls, conf):
        pass

    def __init__(self):
        pass

    @abstractmethod
    def put(self, measure):
        """Sends the given measure.

        Args:
            measure (Measure): the measure to send.
        """
        pass

    def mput(self, measures):
        """Sends a collection of measures.

        Args:
            measures (list): a collection of Measure objects to send.
        """
        # This is the naive implementation.
        # Subclasses may want to override this method to implement a more efficient version (e.g., to avoid
        # opening/closing the connection to the real sink for each call to 'put()'). 
        for measure in measures:
            self.put(measure)

    @classmethod
    def _measure_group(cls, measure):
        return [measure.object_ns, measure.object_id, measure.metric]

    @classmethod
    def _measure_group_encode(cls, measure_group):
        return cls._MEASURE_GROUP_SEP.join([base64.urlsafe_b64encode(group.encode()).decode() for group in measure_group])

    @classmethod
    def _measure_group_decode(cls, measure_group_id):
        return [str(base64.urlsafe_b64decode(enc_group.encode())).decode() for enc_group in measure_group_id.split(cls._MEASURE_GROUP_SEP)]


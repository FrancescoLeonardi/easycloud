##
## Python module for storing measures to Apache Cassandra.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from cassandra import ProtocolVersion
from cassandra.cluster import Cluster, ExecutionProfile, EXEC_PROFILE_DEFAULT
from cassandra.policies import DCAwareRoundRobinPolicy, TokenAwarePolicy, WhiteListRoundRobinPolicy
from core import measures_sink
import json
import logging


class CassandraSink(measures_sink.MeasuresSink):
    """Measures sink that interfaces with Apache Cassandra
       (https://cassandra.apache.org/).

    Inserts collected data into the given keyspace and table (formerly known as
    column family).
    Note, this class expects that both keyspace and table already exists.
    You can create them as follows (where keyspace is 'easycloud' and table is
    'measures'):

        ```
        CREATE KEYSPACE IF NOT EXISTS easycloud WITH replication =
          {'class': 'SimpleStrategy', 'replication_factor' : 3};

        CREATE TABLE IF NOT EXISTS easycloud.measures (
          id uuid PRIMARY KEY, -- Measure ID
          record_ts timestamp, -- When the measure has been recorded
          object_ns text, -- Namespace of the object for which the measure has been collected
          object_id text, -- ID of the object for which the measure has been collected
          metric text, -- The metric name
          unit text, -- The metric unit
          value text); -- The metric value
        ```

    Tested with Apache Cassandra v. 3.11.6 and DataStax's Python Driver v. 3.23.0.

    References:
    - Apache Cassandra: https://cassandra.apache.org/
    - DataStax's Python Driver: https://github.com/datastax/python-driver
    """

    DEFAULT_CONTACT_POINTS = ['localhost'] # A list of contact points to try connecting for cluster discovery
    DEFAULT_PORT = 9042 # The server port
    DEFAULT_KEYSPACE = 'easycloud'
    DEFAULT_TABLE = 'measures'
    DEFAULT_PROTOCOL_VERSION =  ProtocolVersion.MAX_SUPPORTED

    @classmethod
    def from_conf(cls, conf):
        contact_points = None
        if 'contact_points' in conf:
            contact_points = [ cp.strip() for cp in conf['contact_points'].split(',') ]
        port = None
        if 'port' in conf and conf['port'] is not None:
            port = int(conf['port'])
        keyspace = None
        if 'keyspace' in conf:
            keyspace = conf['keyspace']
        table = None
        if 'table' in conf:
            table = conf['table']
        protocol_version = None
        if 'protocol_version' in conf:
            protocol_version = int(conf['protocol_version'])
        return cls(contact_points=contact_points, port=port, keyspace=keyspace, table=table, protocol_version=protocol_version)

    def __init__(self, contact_points=DEFAULT_CONTACT_POINTS, port=DEFAULT_PORT, keyspace=DEFAULT_KEYSPACE, table=DEFAULT_TABLE, protocol_version=DEFAULT_PROTOCOL_VERSION):
        super(CassandraSink, self).__init__()
        self._contact_points = contact_points if contact_points is not None and len(contact_points) > 0 else self.DEFAULT_CONTACT_POINTS
        self._port = port if port is not None and port >= 0 else self.DEFAULT_PORT
        self._keyspace = keyspace if keyspace is not None and len(keyspace) > 0 else self.DEFAULT_KEYSPACE
        self._table = table if table is not None and len(table) > 0 else self.DEFAULT_TABLE
        self._protocol_version = protocol_version if protocol_version is not None and protocol_version >= ProtocolVersion.MIN_SUPPORTED and protocol_version <= ProtocolVersion.MAX_SUPPORTED else self.DEFAULT_PROTOCOL_VERSION
        self._profile = ExecutionProfile(load_balancing_policy=WhiteListRoundRobinPolicy(self._contact_points))


    def put(self, measure):
        measure_key = self._measure_group_encode(self._measure_group(measure))
        try:
            #cluster = Cluster(self._contact_points, port=self._port, load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()))
            cluster = Cluster(self._contact_points,
                              execution_profiles={EXEC_PROFILE_DEFAULT: self._profile},
                              protocol_version=self._protocol_version,
                              port=self._port)
            session = cluster.connect(self._keyspace)
            stmt = session.prepare("INSERT INTO {} (id,record_ts,object_ns,object_id,metric,unit,value) VALUES (uuid(),?,?,?,?,?,?)".format(self._table))
            session.execute(stmt, [measure.timestamp,
                                   measure.object_ns,
                                   measure.object_id,
                                   measure.metric,
                                   measure.unit,
                                   measure.value])
            cluster.shutdown()
        except Exception as e:
            logging.warn('Unable to insert measure: {}'.format(e))

    def mput(self, measures):
        try:
            #cluster = Cluster(self._contact_points, port=self._port, load_balancing_policy=TokenAwarePolicy(DCAwareRoundRobinPolicy()))
            cluster = Cluster(self._contact_points,
                              execution_profiles={EXEC_PROFILE_DEFAULT: self._profile},
                              protocol_version=self._protocol_version,
                              port=self._port)
            session = cluster.connect(self._keyspace)
            stmt = session.prepare("INSERT INTO {} (id,record_ts,object_ns,object_id,metric,unit,value) VALUES (uuid(),?,?,?,?,?,?)".format(self._table))
            for measure in measures:
                session.execute(stmt, [measure.timestamp,
                                       measure.object_ns,
                                       measure.object_id,
                                       measure.metric,
                                       measure.unit,
                                       measure.value])
            cluster.shutdown()
        except Exception as e:
            logging.warn('Unable to insert measure: {}'.format(e))

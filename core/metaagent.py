"""
EasyCloud Metaagent component, used to execute all the commands
the RuleEngine module issues for a specific platform
"""

import logging

from core.actionbinder import get_actions

__author__ = "Davide Monfrecola, Stefano Garione, Giorgio Gambino, Luca Banzato"
__copyright__ = "Copyright (C) 2019"
__credits__ = ["Andrea Lombardo", "Irene Lovotti"]
__license__ = "GPL v3"
__version__ = "0.10.0"
__maintainer__ = "Luca Banzato"
__email__ = "20005492@studenti.uniupo.it"
__status__ = "Prototype"


class MetaAgent:

    def __init__(self, commands_queue, manager, timeout=None):
        """
        Init method (object initialization)

        Args:
            commands_queue (Queue): message queue used for receiving commands from
                                    the platform RuleEngine
            manager (str): The Manager class name
        """
        self.commands_queue = commands_queue
        self.manager = manager
        self._timeout = timeout
        # Set MetaAgent Enabled
        self._stop = False

    def stop(self):
        """
        Stop the MetaAgent
        """
        self._stop = True

    def run(self):
        """
        Main agent loop
        """
        while not self._stop:
            try:
                command = self.commands_queue.get(block=True, timeout=self._timeout)
                logging.debug("Command received: " + str(command))
                self.execute_command(command)
            except queue.Empty as e:
                # Stop waiting for new commands and check if the agent has to be stopped
                logging.debug("Time-out while waiting for a new command")
                #pass
            except Exception as e:
                logging.error("An exception has occourred: " + str(e))
                #pass

    def execute_command(self, command):
        """
        Execute a command issued by the RuleEngine

        Args:
            command (str): The command name
        """
        try:
            all_actions = get_actions(self.manager.__class__.__name__)
            action_method = getattr(self.manager, all_actions[command["action"]])
        except AttributeError:
            logging.error("Error: the " + self.manager.platform_name + " module has no action called " + command["action"])
        except Exception as e:
            logging.error("An exception has occourred: " + str(e))
        #logging.debug("Executing \"" + command["action"] + "\" for instance " + command["instance_id"])
        action_method(command["instance_id"])

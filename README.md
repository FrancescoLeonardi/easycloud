# EasyCloud
![EasyCloud logo](EasyCloudLogo.jpg)

|Website|Download|Wiki|Report|License|Issues|
|---------|---------|---------|---------|----------|----------|
| [![Website Button](https://img.shields.io/badge/Open-Website-green.svg)](https://subwave07.github.io/easycloud)| [![Download Button](https://img.shields.io/badge/EasyCloud-0.10.0-blue.svg)](../../archive/master.zip) | [![Wiki Button](https://img.shields.io/badge/Open-Wiki-brightgreen.svg)](../../wiki) | [![Thesis Button](https://img.shields.io/badge/Report-Italian-yellow.svg)](dummy) | [![GNU v3 License Button](https://img.shields.io/badge/License-GNU%20v3-green.svg)](LICENSE) | [![Report Problems Button](https://img.shields.io/badge/Report-Problems-red.svg)](issues)|


EasyCloud is a text user interface written in Python able to interact with
multiple cloud platforms, such as OpenStack, Amazon Web Services (AWS) and Google
Cloud Platform (GCP). <!-- It is based on the [CloudTUI-FTS](https://github.com/mrbuzz/CloudTUI-FTS) project. -->

With EasyCloud, a user can:  
* start/stop/clone a VM
* manage floating IPs and volumes
* monitor the VMs health status
* gather metrics data from instances and send them into multiple sinks (e.g., save them in plain CSV files or in a Redis data store, or publish them to Apache Kafka)
* create/manage policies in order to prevent faults (i.e., "if the CPU utilization is higher than XX %, then clone it")

## Quick start:

1. **Requirements**  
    You need an account for the cloud platform you want to use. Some cloud platform requires
    a payment for some service they provide, so we suggest to consult all the prices listings before
    using EasyCloud.
    Python 3.7 and pip3 19.0 are required in order to use this tool.
    Please note all the tests were conducted on these versions, so it is possible
    that an older or newer version of Python 3 can still run this program.

2. **Install the required core libraries**  
    This step can be done manually or autotically with the installer script provided. In both of the cases, pip3
    must be installed prior to proceeding further.  

    * **[Automatic installation]** If you want to use the installer, move to the `EasyCloud/libs_installer` directory and execute

        `./installer.sh` 

        Wait until the end of the process. If an error occourred during the installation, a log file will be
        available in the `libs_installer` directory, containing the cause of failure.

    * **[Manual installation]** If you want to perform a manual installation, open a terminal and run the following command

        `pip3 install --user -r requirements.txt`

3. **Set up the configuration file**

    Each cloud platform provides the credentials to enable the user to interact with the platform services. You need to fill up these credentials into the
    specific configuration of the cloud platform you want to use. The configuration files are located in modules/*platform*/settings.cfg
    where platform is one the following options: *aws\_libcloud* for AWS, *gcp_libcloud* for GCP, *chameleon\_libcloud* for OpenStack provided by Chameleon project
    and *openstack\_libcloud* for the vanilla OpenStack cloud platform. Each setting file has some mandatory settings and some optional setting as exaplained
    below.

* **[Mandatory settings]**
    * **[Amazon Web Services]** 
    First of all, go to the specific directory for AWS:
    `cd modules/aws_libcloud/`
    and create a local copy of the config file:
    `cp settings.cfg.template settings.cfg`
    Follow the instuction provided in the settings.cfg file to fill up at least the first 3 parameters that are mandatory: *ec2\_access\_key\_id*, *ec2\_secret\_access_key* and *ec2\_default\_region*.
    
    * **[Google Cloud Platform (GCP)]**
    First of all, go to the specific directory for GCP:
    `cd modules/gcp_libcloud/`
    and create a local copy of the config file:
    `cp settings.cfg.template settings.cfg`
    Follow the instuction provided in the settings.cfg file to fill up at least the first 3 parameters that are mandatory: *gcp\_access\_key\_id*, *gcp\_secret\_access_key* and *gcp\_project*.
    
    * **[OpenStack by Chameleon Project]**
    First of all, go to the specific directory for OpenStack:
    `cd modules/chameleon_libcloud/`
    and create a local copy of the config file:
    `cp settings.cfg.template settings.cfg`
    Follow the instuction provided in the file to set up at least the first 6 parameters that are mandatory: *os\_auth\_key\_url*, 
    *os\_username*, *os\_password*, *os\_project\_name*, *os\_project\_id* and *os\_region*.
    
    Once you have filled up the mandatory parameters, you are ready to use cloud platform selected (you can skip the "optional settings" below) by following
    the instruction described in "Run EasyCloud" section.

3. **Optional settings**  
    You can then customize the platform specific settings (if available) in **modules/*platform*/settings.cfg**.
    For example, each platform can have a limited free-tier mode available and this mode must be enable through a variable
    in order to filter certain types of resources, based on the free-tier conditions.  In the settings.cfg file, you will find a short guide
    to set up properly the optional settings.

4. **Run EasyCloud** using `easycloud_launcher.sh` on macOS, BSD and GNU/Linux distributions and then follow the instructions provided by the tool.
    If a missing package is detected, EasyCloud won't start or will ask if it can download and install it through pip3. 

5. **Flags**  
    EasyCloud shell script can be launched with certain flags, in order to perform the following operations:
    
    * **--debug**  
        save all the outgoing HTTP requests and all the incoming HTTP responses made by Libcloud in `logs/libcloud_debug.log`
        This setting works only if the application has been executed using the shipped shell script.
    
    * **--no_libs_check**  
        disable the dependencies check for each module at the start of EasyCloud

    * **--help**  
         display a list of available flags

    * **--version**  
        display the application version

6. **Debug messages**  
    This application will record all the debug messages in `easycloud.log`, in order to study its behaviour and the Monitor and Rule Engine iterations.

## Wiki

Please, consult the [Wiki](../../wiki) for a more detailed explanation on how to configure EasyCloud.

## Supported platforms

![Platforms logos](README.md_files/providers.jpg)

The following platforms are currently supported by EasyCloud

* **[Amazon Web Services](https://aws.amazon.com)**
* **[Chameleon Cloud](https://www.chameleoncloud.org) (based on Openstack)**
* **[Google Cloud Platform](https://cloud.google.com)**
* **[Openstack](https://www.openstack.org)**

## Source code download

You can download the application source code from our git repository by using the `git clone git@gitlab.di.unipmn.it:DCS/easycloud.git` command.

<!--You can download the application source code from our git repository by using the `git clone <URL>` command,
where `<URL>` has to be replaced with one of the following:

0.10.0 (Preview)(**Current**) - `https://github.com/subwave07/easycloud.git`  
0.08.0 (CloudTUI-FTS, Preview) - `https://github.com/mrbuzz/CloudTUI-FTS.git`  
0.06.0 (CloudTUI-FTS, Preview) - `https://github.com/trampfox/CloudTUI-FTS.git`
-->

## License
This project is available under the GNU General Public License v3. A copy of this license is shipped with the tool and available [here](LICENSE).

## Institute

![University Logo](README.md_files/upo_logo.png)

This project is a collaborative effort made by a group of students of the

**University of Eastern Piedmont Amedeo Avogadro  
Department of Science and Innovation Technology (DiSIT)  
Alessandria and Vercelli, Piedmont, Italy  
[https://www.disit.uniupo.it](https://www.disit.uniupo.it)**

### Credits
* Andrea Lombardo (for the original CloudTUI project)
* Davide Monfrecola (for the CloudTUI-FTS project)
* Giorgio Gambino (for the CloudTUI-FTS project)
* Irene Lovotti (for the original CloudTUI project)
* Luca Banzato (for the original EasyCloud project)
* Stefano Garione (for the CloudTUI-FTS project)

### Superadvisor
* Massimo Canonico

### Contributors
* Marco Guazzone [marco.guazzone@uniupo.it](mailto:marco.guazzone@uniupo.it)

### Contact info
[massimo.canonico@uniupo.it](mailto:massimo.canonico@uniupo.it)

<!--## Papers

EasyCloud and CloudTUI-FTS have been presented and referenced in the following academic papers:

* **[CloudTUI: a multi Cloud platform Text User Interface](Papers/paper1_cloudtui.pdf)**  
    Canonico M., Lombardo A., Lovotti I.  
    In "7th International Conference on Performance Evaluation Methodologies and Tools, ValueTools '13", pp. 294-297.  
    ICST/ACM  
    2013

* **[CloudTUI-FTS: a user-friendly and powerful tool to manage Cloud Computing Platforms](Papers/paper2_cloudtui.pdf)**  
    Canonico M., Monfrecola D.  
    In "Proceedings of the 9th EAI International Conference on Performance Evaluation Methodologies and Tools", pp. 220-223.  
    ICST (Institute for Computer Sciences, Social-Informatics and Telecommunications Engineering)  
    2016

* **[CIMP: Cloud Integration and Management Platform](Papers/reference1_cloudtui.pdf) (Ref.)**  
    Sefraoui O., Aissaoui M. and Eleuldj M.,
    In "Europe and MENA Cooperation Advances in Information and Communication Technologies", pp. 391-400.  
    Springer, Cham  
    2017
-->
